using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.src.Context;
using tech_test_payment_api.src.Models;

namespace tech_test_payment_api.src.Controller
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendedorController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost("CriarVendedor")]
        public IActionResult CriarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return CreatedAtAction("CriarVendedor", new { id = vendedor.Id }, vendedor);
        }

        [HttpGet("ObterTodosOsVendedores")]
        public IActionResult ObterTodos()
        {
            return Ok(_context.Vendedores);
        }
       
    }
}
