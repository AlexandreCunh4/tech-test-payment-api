using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.src.Context;
using tech_test_payment_api.src.Models;

namespace tech_test_payment_api.src.Controller
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistrarVenda(Venda venda)
        {
            
            if(venda.Quantidade==0)
            return BadRequest(new { Erro = "Favor informar a quantidade do produto desejada!!" });


            _context.Add(venda);
            _context.SaveChanges();

            return Ok(venda);
        }

        [HttpGet("ObterVendaPor{id}")]
        public IActionResult ObterVendaPor(int id)
        {


            var vendas =
            from v in _context.Vendas
            join ve in _context.Vendedores on v.VendedorId equals ve.Id
            where v.VendaId.Equals(id)
            orderby v.VendaId

            select new
            {
                NumeroDaVenda = v.VendaId,
                Produto = v.ItemVenda,
                Vendedor = ve.Nome,
                StatusVenda = v.Status,
                
            };

            return Ok(vendas);


        }


        [HttpPut("AtualizarPedido")]

        public IActionResult Atualizar(int id, EnumStatusVenda status)
        {

            var vendaBanco = _context.Vendas.Find(id);


            if (vendaBanco == null)
                return NotFound();


            if (vendaBanco.Status == EnumStatusVenda.Entregue)
                return BadRequest(new { Erro = "O status não pode ser atualizado, pois o pedido já conta como entregue" });
            else
            {
                if (vendaBanco.Status == EnumStatusVenda.EnviadoParaTransportadora && status == EnumStatusVenda.Cancelada)
                    return BadRequest(new { Erro = "O status não pode ser atualizado, pois o mesmo não está seguindo a regra de atualização do Pedido" });
                else
               if (vendaBanco.Status == EnumStatusVenda.AguardandoPagamento && status == EnumStatusVenda.PagamentoAprovado || status == EnumStatusVenda.Cancelada)
                    vendaBanco.Status = status;
                else
                   if (vendaBanco.Status == EnumStatusVenda.PagamentoAprovado && status == EnumStatusVenda.EnviadoParaTransportadora || status == EnumStatusVenda.Cancelada)
                    vendaBanco.Status = status;
                else
                   if (vendaBanco.Status == EnumStatusVenda.EnviadoParaTransportadora && status == EnumStatusVenda.Entregue)
                    vendaBanco.Status = status;

                else
                    return BadRequest(new { Erro = "O status não pode ser atualizado, pois o mesmo não está seguindo a regra de atualização do Pedido" });

            }

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);

        }

    }
}