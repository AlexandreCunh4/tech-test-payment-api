using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.src.Models
{
    public class Venda
    {
        public int VendaId { get; set; }
        public int VendedorId { get; set; }
        public string ItemVenda { get; set; }
        public int Quantidade { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda Status { get; set; }

    }
}